<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $rows;
    public function __construct()
    {
        $this->rows = [
            [
                [
                    'id' => 1,
                    'name' => 'A',
                    'value' => 1
                ],
                [
                    'id' => 2,
                    'name' => 'B',
                    'value' => 1
                ],
            ],
            [
                [
                    'id' => 3,
                    'name' => 'C',
                    'value' => 1
                ],
                [
                    'id' => 4,
                    'name' => 'D',
                    'value' => 1
                ],
                [
                    'id' => 5,
                    'name' => 'E',
                    'value' => 1
                ],
                [
                    'id' => 6,
                    'name' => 'F',
                    'value' => 1
                ],
                [
                    'id' => 7,
                    'name' => 'G',
                    'value' => 1
                ]
            ],
            [
                [
                    'id' => 8,
                    'name' => 'H',
                    'value' => 1
                ]
            ]
        ];
    }
    public function index()
    {
        $buttonRows = $this->rows;

        $user = [
            'name' => 'Nama User',
        ];

        $data = [
            'button_rows' => $buttonRows,
            'user'  => $user
        ];

        return view('home', $data);
    }

    public function calculate(Request $request)
    {
        $buttons = $request->input('button_ids', []);
        $value = 0;
        if (in_array(3, $buttons)) {
            foreach ($buttons as $btn) {
                if (in_array($btn, [3,4,5,6,7])) {
                    $value++;
                }
            }
        }
        $finalValue = 0;

        if ($value > 0) {
            if (in_array(3, $buttons)) {
                $finalValue = 1/$value;
                $finalValue = number_format((float)$finalValue, 4, '.', '');
            } else {
                $finalValue = 0/$value;
            }
        }
        // dd($finalValue);
        return response()->json([
            'data' => [
                'value' => $finalValue
            ],
            'success' => true
        ]);
    }
}
