### REQUIREMENT:
- PHP7.3 | PHP8
- Composer

### HOW TO INSTALL
#### Direct (without apache)
- clone the repository under `master` branch
- go to the directory
- click `composer install`
- click `php artisan serve`

#### With Apache or Other WebServer
- clone the repository under `master` branch
- move the cloned repository to webserver directory
- go to the directory
- click `composer install`
- set the webserver configuration the rootDirectory to use `{directoryLocation}/public` | `change the {directoryLocation} with correct path`
- check in the browser with type `localhost/{directoryName}`

#### Tutorial or Documentation
- [https://diszaid.medium.com/instalasi-laravel-di-windows-paket-kumplit-instal-xampp-composer-sampe-laravel-e903582842e]
- [https://divpusher.com/blog/how-to-run-laravel-on-windows-with-xampp/]