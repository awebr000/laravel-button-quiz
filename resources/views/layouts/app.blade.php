<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- jquery -->
    @yield('custom-style')
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">

</head>
<body>
@yield('container')

<!-- jquery -->
<script src="{{asset('js/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
@yield('custom-script')
</body>
</html>