@extends('layouts.app')
@section('title', 'Home')

@section('custom-style')
<style>
.this-btn {
    width: 100px;
    height: 100px;
    line-height: 1.7 !important;
    font-size: 3rem !important;
}
#result {
    display: none;
}
#result-val {
    font-size: 4rem;
}
.selected {
    background-color: #3f6db3 !important;
}
.closed {
    border-right: 1px solid black;
}
.history {
    padding: 0 5px;
}
</style>
@endsection

@section('container')
<div class="container">
    <div class="mt-5">
        <div>
            <p class="m-0">Welcome, <strong>{{$user['name']}}</strong></p>
            <span id="date">Wednesday, 12 Januari 2021</span> | <span id="clock">-</span>
        </div>
        <div class="row mt-5 pt-5">
            <div class="col-lg-9">
                @foreach($button_rows as $row)
                <div class="row mb-5">
                    @foreach($row as $button)
                    <div class="col-lg-1 me-5">
                        <div data-id="{{$button['id']}}" data-name="{{$button['name']}}" class="btn btn-primary this-btn">
                            {{$button['name']}}
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <div id="btn-history" class="d-flex">
                    
                </div>
            </div>
            <div class="col-lg-3" id="result">
                <div class="btn btn-primary mb-4 form-control undo">undo</div>
                <div class="card pt-3 pb-3 pe-4 ps-4">
                    <div class="card-body text-center">
                        <div class="card-title">
                            <h3>Selamat!</h3>
                        </div>
                        <div class="card-text">
                            Hasil yang anda terima adalah<br>

                            <span id="result-val"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script src="{{asset('js/moment/moment.min.js')}}"></script>
<script>
$(function() {
    var time = moment().format('H:mm:ss');
    $('#clock').html(time);

    function updateTime () {
        var time = moment().format('H:mm:ss');
        $('#clock').html(time);
    }
    setInterval(updateTime, 1000);

    var date = moment().format('dddd, DD MMMM YYYY');
    $('#date').html(date);
});
</script>

<script>
$(function() {
    function clickedButton (show = true) {
        var payload = new FormData();
        var url = '{{route('home.calculate')}}'

        // var selected = $('.btn-value');
        var selected = $('.history').last().children('span')
        selected.map(function(select) {
            payload.append('button_ids[]', $(this).data('id'))
        })

        $.ajax({
            url: url,
            data: payload,
            dataType: 'json',
            type: 'post',
            contentType: false,
            processData: false,
            success:function (res) {
                if (show) {
                    $('#result').show()
                } else {
                    $('#result').hide()
                }
                $('#result-val').html( (res.data.value*100).toFixed(2) + '%' );
            },
            error:function(e) {
            }
        })
    }

    $('.this-btn').on('click', function(e) {;
        var step = $('.this-btn[data-step]').length;

        if ($('#btn-history').children('.open').length == 0) {
            $('#btn-history').append('<div class="history open"></div>')
        }

        if ($(this).data('name') != 'H') {
            $('.open').append('<span data-id="'+ $(this).data('id') +'" class="btn-value">'+ $(this).data('name') +'</span>')
        } else {
            $('.open').append('<span data-id="'+ $(this).data('id') +'" class="btn-value">'+ $(this).data('name') +'</span>')
            $('.open').addClass('closed')
            $('.open').removeClass('open')
        }

        clickedButton();
    });

    $('.undo').on('click', function(e) {
        if ($('.history').last().children('span').length == 0) {
            $('.history').last().remove();
        }

        if ($('.history').last().hasClass('closed')) {
            $('.history').last().removeClass('closed');
            $('.history').last().addClass('open');
        }
        $('.open span:last-child').remove();
        clickedButton();
    });
})
</script>
@endsection